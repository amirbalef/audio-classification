import numpy as np
from torch.utils.data import Dataset
from torchvision import transforms

class ESC50Data(Dataset):
  def __init__(self, base,foldNumber, augmentation):
    feat = np.load("../audio-features/features/esc50-feat-uint8.npy")
    labels = np.load("../audio-features/features/esc50-labels.npy")
    labels = labels.reshape(feat.shape[0],-1).astype(np.int)
    self.transform = None
    if(augmentation):
        transform = transforms.Compose([
        transforms.ToPILImage(),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor()
        ])
        self.transform = transform
    if(base=="train"):
        self.data =  feat[labels[:,1]!=foldNumber]
        self.data = np.expand_dims(self.data, axis=1)
        self.labels = labels[labels[:,1]!=foldNumber,0]
    else:
        self.data =  feat[labels[:,1]==foldNumber]
        self.data = np.expand_dims(self.data, axis=1)
        self.labels = labels[labels[:,1]==foldNumber,0]
  def __len__(self):
    return len(self.data)
  def __getitem__(self, idx):
    if(augmentation):
        item = self.data[idx]
        item = self.transform(item)
        return item, self.labels[idx]
    else:
        return self.data[idx], self.labels[idx]


class ESC50DataMixup(Dataset):
  def __init__(self, base,foldNumber):
    feat = np.load("../audio-features/features/esc50-feat-uint8.npy")
    labels = np.load("../audio-features/features/esc50-labels.npy")
    labels = labels.reshape(feat.shape[0],-1).astype(np.int)
    if(base=="train"):
        self.data1 =  feat[labels[:,1]!=foldNumber]
        self.data1 = np.expand_dims(self.data1, axis=1)/255.0
        self.data2 = self.data1.copy()
        np.random.shuffle(self.data2)
        self.labels = labels[labels[:,1]!=foldNumber,0]
    else:
        self.data1 =  feat[labels[:,1]==foldNumber]
        self.data1 = np.expand_dims(self.data1, axis=1)/255.0
        self.data2 = self.data1.copy()
        np.random.shuffle(self.data2)
        self.labels = labels[labels[:,1]==foldNumber,0]
  def __len__(self):
    return len(self.data1)
  def __getitem__(self, idx):
    return (self.data1[idx]+self.data2[idx])/2, self.labels[idx]
