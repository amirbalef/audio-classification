from comet_ml import Experiment

import os
import argparse

import numpy as np
np.random.seed(0)

import torch
torch.manual_seed(0)

from torch.utils.data import DataLoader
import torch.optim as optim
import torch.nn as nn

from Models import resnet34, resnet18, mobilenetv3, acnet
from datasets import ESC50Data, ESC50DataMixup
from train import train, lr_decay


def main(hyper_params):

    num_classes = 50
    foldNumber =1
    epochs = hyper_params['epochs']
    batch_size = hyper_params['batch_size']
    learning_rate = hyper_params['learning_rate']
    
    # Create an experiment with your api key:
    experiment = Experiment(
        api_key=hyper_params['api_key'],
        project_name="audio-classification",
        workspace="amirbalef",
    )
    experiment.log_parameters(hyper_params)

    train_data =ESC50Data('train',foldNumber,augmentation=True)
    valid_data =ESC50Data('valid',foldNumber,augmentation=False)
    train_loader = DataLoader(train_data, batch_size=batch_size, shuffle=True)
    valid_loader = DataLoader(valid_data, batch_size=batch_size, shuffle=True)

    #trainMix_data =ESC50DataMixup('train',foldNumber)
    #trainMix_loader = DataLoader(trainMix_data, batch_size=batch_size, shuffle=True)
    
    if torch.cuda.is_available():
      device=torch.device('cuda:0')
    else:
      device=torch.device('cpu')

    if hyper_params['model']== 'resnet34':
        model = resnet34.resnet34_model(num_classes=num_classes)
    if hyper_params['model']== 'resnet18':
        model = resnet18.resnet18_model(num_classes=num_classes)
    if hyper_params['model']== 'acnet':
        model = acnet.acnet_model(num_classes=num_classes)    
    if hyper_params['model']== 'mobilenetv3':
        model = mobilenetv3.mobilenetv3_small(num_classes=num_classes,width_mult=0.25)    

    model = model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)

    loss_fn = nn.CrossEntropyLoss()
    
    #for iteration in range(epochs//10 -1):
        #train(device, model, loss_fn, trainMix_loader, valid_loader, 8, optimizer, lr_decay, experiment)
    #    train(device, model, loss_fn, train_loader, valid_loader, 2, optimizer, lr_decay, experiment)
    train(device, model, loss_fn, train_loader, valid_loader, epochs, optimizer, lr_decay, experiment)    
    
    #with open('esc50resnet.pth','wb') as f:
    #    torch.save(model, f)
    


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--api_key", type=str)
    parser.add_argument("--dataset", type=str, default="ESC-50", choices=["ESC-50"])
    parser.add_argument("--model", type=str, default="resnet34", choices=["resnet18","resnet34","mobilenetv3","acnet"])
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--learning_rate", type=float, default=2e-4, help="learning rate")
    parser.add_argument("--epochs", type=int, default=20)
    parser.add_argument("--optimizer", type=str, default="ADAM",choices=["ADAM"])
    args = parser.parse_args()

    print("=" * 80)
    print("Summary of training process:")
    print("Batch size: {}".format(args.batch_size))
    print("Learing rate       : {}".format(args.learning_rate))
    print("Number of epochs       : {}".format(args.epochs))
    print("Dataset       : {}".format(args.dataset))
    print("Local Model       : {}".format(args.model))
    print("=" * 80)

    hyper_params = {
        "api_key": args.api_key,
        "dataset": args.dataset,
        "model": args.model,
        "batch_size": args.batch_size,
        "learning_rate": args.learning_rate,
        "epochs": args.epochs,
        "optimizer": args.optimizer
    }
    main(hyper_params)
