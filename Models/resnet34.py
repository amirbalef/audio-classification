import torch
import torch.nn as nn
from torchvision.models import resnet34
def resnet34_model(num_classes):
    resnet_model = resnet34(pretrained=True)
    resnet_model.fc = nn.Linear(512,num_classes)
    resnet_model.conv1 = nn.Conv2d(1, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
    return resnet_model
